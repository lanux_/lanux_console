# README #
  
# Console chat dokumentace #
## hlavní princip navigace: ##
Každé view má 2 hlavní metody:  
  
1. ``public void NameMode()`` - Ovladač / spouštěč metod v daném View  
2. ``public void DrawName()`` - Grafický výstup pro uživatele  
  
## Controller ##
*Řídí běh celého projektu, slouží jako ovladač*  
  
  
**Delegate ActiveMode** - Aktivní Mode 
   
**Delegate ActiveView** - Aktivní vykreslovací metoda  
  
  
  
``public void Controls()`` - Běží v threadu. Vládání šipkami, Esc-zavře, Enter-zvolí  
  
``public string getMessageFromUser()`` - Spouští thread Keyboard, kde uživatel píše a to vrací jako string  
  
``private void Keyboard()`` - Běží v threadu. Simulátor klávesnice, který ukládá do stringu po jedné klávese  
  
``public void Modes()`` - cyklí v něm aktivní mód  
  
``public void DrawOptions(List<string> options)`` - Vykreslí list možností, které pošle View  
  
## MainMenu ##
*Nechá uživatele vybrat mezi LogIn a SignUp*  
  
## LogIn ##
* Po přihlášení pošle do CrossRoad  
  
``private bool LogIn()`` - provede uživatele loginem a vrátí, jestli se povedlo  
  
``private bool ApiLogIn(User u)`` - Poslání vytvořeného "uživatele" na ověření na Api  
  
``private User ApiCurrentUser(string username)`` - Nechá si z Api poslat přihlášeného usera a uloží ho v controlleru  
  
## SignUp ##
* Kontroluje heslo regexem  
* kontroluje dostupnost  
* Po vytvoření přihlásí a pošle do Crossroad  
  
``private bool RegistationController()`` - spustí SignUp, spouští metody  
  
``private Basic.User CreateNewUser()`` - provede uživatele vytvořením nového usera  
  
``private bool ApiLogIn(User u)`` - přihlásí právě vytvořeného uživatele 
  
``private User ApiCurrentUser(string username)`` - Nechá si z Api poslat přihlášeného usera a uloží ho v controlleru  
  
``private bool PasswordValidation(string psswd1, string psswd2)`` - zkontroluje, zda-li je heslo validní  
  
## CrossRoad ##
*Rozcestník, slouží jako místo, kam se může uživatel vracet*  
  
* Esc zavírá aplikaci  
  
## JoinChatroom ##
* Nechá vybrat uživatele z chatroomek, ve kterých je členem*  
  
* Esc vrací do crossroad  
* Přesměruje do Chat  
  
``private List<Basic.Chatroom> ApiChatroomList()`` - Nechá si z api poslat seznam chatroomek, ve kterých je členem  
  
``private void DrawChatrooms(List<Basic.Chatroom> list)`` - Vypíše Chatroomy  
  
## Friends ##
*List přátel*  
  
* Esc vrací do crossroad  
* Vypíše všechny uživatele, přátele znázorní zeleně  
* Kontroluje, jestli si uživatel nepřidal sebe, přítele, nebo neexistujícího usera  
  
``private void DrawUsers()`` - Vykreslí uživatele a znázorní přátele  
  
``AddFriend()`` - Průvodce pro přidání nového přítele  
  
``private bool ApiAddFriend(string FriendsUsername)`` - Pošle na Api příkaz pro přidání přítele  
    
``private List<Basic.User> ApiFriendList()`` - Nechá si z api poslat seznam svých přátel  
  
## Chat ##
* Esc vrací do crossroad  
* Místnost pro samotné chatování  
* Nejde poslat prázdná zpráva  
  
``private void ApiUpdate()`` - Zkontroluje nové zprávy, kdyžtak vypíše  
  
``private List<Message> ApiGetMessageListById(int idChatroom)`` - Dostane všechny zprávy v aktuálním chatroomu  
  
``private void ApiSendMessage(string content)`` - pošle zprávu  
  
## CreateChatroom ##
*Vytvoří chatroom*  
  
* Esc vrací do crossroad  
* Přesměruje do EditChatroom  
  
``private void CreateRoom()`` - provede uživatele vytvořením chatroomy  
  
``private void ApiGetChatroomByName()`` - Stáhne právě vytvřenou chatroom  
  
``private bool ApiCreateChatroom(string name)`` - Vytvoří chatroom  
  
## EditChatroom ##
*Nechá uživatele vybrat jednu z jeho chatroomek a přidávat do ní přátele*  
  
* Vypíše Usery, kteří jsou v chatroomce  
* Vypíše přátele  
* Ty co jsou už členy, zvýrazní zeleně  
* Kontroluje duplicity  
* Nemůže přidat nikoho, koho nemá v přátelích  
  
``private void GetFriendIdFromUser()`` - Provede uživatele přidáním frienda  
  
``private void FIdValidation(int id)`` - Ověří, zda user existuje, jestli  už není v listu, jestli je v přátelích atd..  
  
``private void DrawUserList()`` - Vypíše list userů, co jsou v chatroomu + barvy  
  
``private void ApiGetUsersByChatroom()`` - Dostane list userů v chatroomu  
  
``private bool ApiSendUsersToChatroom()`` - pošle list přidaných přátel do chatroomu
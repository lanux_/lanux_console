﻿using CMDuxv2.Basic;
using CMDuxv2.Views;
using CMDuxv2.Views.objects;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;
using WindowsInput.Native;

namespace CMDuxv2
{
    public class Controller
    {

        //views
        public static Chat _Chat;
        public static MainMenu _MainMenu = new MainMenu();
        public static Login _Login = new Login();
        public static CrossRoad _CrossRoad = new CrossRoad();
        public static JoinChatroom _JoinChatroom;
        public static SignUp _SignUp = new SignUp();
        public static Friends _Friends;
        public static CreateChatroom _CreateChatroom = new CreateChatroom();
        public static EditChatroom _EditChatroom;
        //delegates
        public static ActiveDelegate DlgActive = new ActiveDelegate();
        //thread control
        public static bool IsKeyboardActive = false; //is user typing - true
        public static bool ThrdControls = true;
        public static bool ThrdModes = true;
        public static bool ThrdUpdate = true;
        public static bool BoolChat = false;
        //keyboard
        public static string KeyboardOutput = "";
        public static bool IsKeyboardOutputAviable = false;
        //junk
        public static int HighlightAtIndex = 0; // active option
        public static User LocalUser = new User();
        public static bool GetPasswd = false;
        public static string Gap = "     ";
        public static bool UseEscAsExit = true;
        //change detectors
        public static bool SelectHighlight = false;
        public static bool HighlightChange = false;

        public void Controls() //Controls ARROWS, ESC, ENTER
        {
            ConsoleKey key;
            while (ThrdControls)
            {
                if(!IsKeyboardActive)
                {
                    key = Console.ReadKey().Key;
                    if (key == ConsoleKey.Escape)
                    {
                        if (UseEscAsExit)
                            Environment.Exit(0);
                        else
                        {
                            DlgActive.ChangeActive(_CrossRoad.CrossRoadMode, _CrossRoad.DrawCrossRoad);
                            DlgActive.View();
                        }
                    }

                    else if (key == ConsoleKey.UpArrow)
                    {
                        HighlightAtIndex = HighlightAtIndex - 1;
                        HighlightChange = true;
                    }
                    else if (key == ConsoleKey.DownArrow)
                    {
                        HighlightAtIndex = HighlightAtIndex + 1;
                        HighlightChange = true;
                    }

                    else if (key == ConsoleKey.Enter)
                    {
                        SelectHighlight = true;
                        HighlightChange = true;
                    }
                }
            }
        }

        public string GetMessageFromUser(bool Passwd) //Simulates Active Keyboard... UNTIL USER HITS ENTER
        {
            IsKeyboardActive = true;

            if (Passwd)
                GetPasswd = true;

            Thread tKeyboard = new Thread(new ThreadStart(Keyboard));
            tKeyboard.Start();

            while (true)
            {
                if (IsKeyboardOutputAviable)
                {
                    string tmp = KeyboardOutput;          //
                    IsKeyboardOutputAviable = false;      //Stopy pod koberec
                    KeyboardOutput = "";                  //
                    if (Passwd)
                        GetPasswd = false;
                    IsKeyboardActive = false;
                    return tmp;
                }
            }

        }
        
        private void Keyboard() //Thread Methot which simulates keyboard
        {
            HitEnter();
            KeyboardOutput = "";
            ConsoleKeyInfo keyPressed;
            while (true)
            {
                keyPressed = Console.ReadKey();

                switch (keyPressed.Key)
                {
                    case ConsoleKey.Escape: //jestli je v chatroom, vrati se do crossroad
                        if (BoolChat)
                        {
                            BoolChat = false;
                            KeyboardOutput = "";
                            break;
                        }
                        continue;
                    case ConsoleKey.Backspace:
                        KeyboardOutput = BackSpace(KeyboardOutput);
                        ClearCurrentConsoleLine();
                        DrawKeyboardOutput(KeyboardOutput);
                        continue;
                    case ConsoleKey.Enter:
                        break;
                    default:
                        if (keyPressed.Key != ConsoleKey.Escape)
                        {
                            KeyboardOutput += keyPressed.KeyChar.ToString();
                            ClearCurrentConsoleLine();
                            DrawKeyboardOutput(KeyboardOutput);
                        }
                        continue;
                }
                break;
            }
            IsKeyboardOutputAviable = true;
        }

        public static string BackSpace(string str)
        {
            if (string.IsNullOrEmpty(str))
                return str;
            else
                return str.TrimEnd(str[str.Length - 1]);
        }

        private void DrawKeyboardOutput(string content)
        {
            Console.Write(Gap);
            if (GetPasswd)
            {
                for (int i = 0; i < content.Length; i++)
                    Console.Write("*");
            }
            else
                Console.Write(content);
        }
        public void Modes()    //Modes
        {
            //ActiveView();
            DlgActive.View();

            while(ThrdModes)
                DlgActive.Mode();
        }

        public void DrawOptionsMenu(string[] Options) //Draw options menu
        {
            int i = 0;
            foreach (string Item in Options)
            {
                if (i == HighlightAtIndex)
                    Highlight(Item);
                else
                    Console.WriteLine(Gap + Item);
                i++;
            }
        }

        public void DrawOptions(List<string> options) //Draw options menu
        {
            int i = 0;
            foreach (string Item in options)
            {
                if (i == HighlightAtIndex)
                    Highlight(Item);
                else
                    Console.WriteLine(Gap + Item);
                i++;
            }
        }

        public void Highlight(string content)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.BackgroundColor = ConsoleColor.Gray;
            Console.WriteLine(Gap + content);
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
        }

        //
        // mini methods
        //

        public void RecalculateHighlight(int OptionsCount) //makes border for highlight  //nemůžeš najet na to, co tam není
        {
            if (HighlightAtIndex > OptionsCount - 1)
                HighlightAtIndex = OptionsCount - 1;
            else if (HighlightAtIndex < 0)
                HighlightAtIndex = 0;
        }

        private bool BackToCrossRoad() //navigates back to crossRoad
        {
            if (DlgActive.Mode == _Chat.ChatMode) //    || ActiveMode == _CreateChatRoom._CreateChatRoomMode
            {
                if (DlgActive.Mode == _JoinChatroom.JoinChatRoomMode)
                {
                    DlgActive.ChangeActive(_CrossRoad.CrossRoadMode, _CrossRoad.DrawCrossRoad);
                    ThrdUpdate = false;
                    return true;
                }
            }
            return false;
        }

        public void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }

        public void DrawMoon()
        {
            Console.WriteLine(@"                                                          ___---___  " + "\n" +
                              @"                                                       .--         --." + "\n" +
                              @"                                                     ./   ()      .-. \." + "\n" +
                              @"                                                    /   o    .   (   )  \" + "\n" +
                              @"                                                   / .            '-'    \" + "\n" +
                              @"                                                  | ()    .  O         .  |" + "\n" +
                              @"                                                 |          TEAM_D         |" + "\n" +
                              @"                                                 |    o           ()       |" + "\n" +
                              @"                                                 |       .--.          O   |" + "\n" +
                              @"                                                  | .   |    |            |" + "\n" +
                              @"                                                   \    `.__.'    o   .  /" + "\n" +
                              @"                                                    \                   /" + "\n" +
                              @"                                                     `\  o    ()      /'" + "\n" +
                              @"                                                       `--___   ___--'" + "\n" +
                              @"                                                             ---");
        }

        public void HitEnter()
        {
            InputSimulator a = new InputSimulator();
            a.Keyboard.KeyPress(VirtualKeyCode.RETURN); //simulates keystroke
        }

        public string GapByLenght(int len)
        {
            string gap = "";
            for (int i = 0; i < len; i++)
            {
                gap += " ";
            }
            return gap;
        }
    }
}

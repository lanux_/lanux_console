﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CMDuxv2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WindowHeight = 30;
            Console.WindowWidth = 120;
            Console.BufferWidth = 120;
            Console.BufferHeight = 9001;

            Controller ctrl = new Controller();
            Thread tModes = new Thread(new ThreadStart(ctrl.Modes));
            Thread tControls = new Thread(new ThreadStart(ctrl.Controls));

            tControls.Start();
            tModes.Start();
        }
    }
}

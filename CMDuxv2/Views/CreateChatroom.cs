﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CMDuxv2.Views
{
    public class CreateChatroom : Controller
    {
        private HttpClient client = new HttpClient();
        Basic.Chatroom LocalChatroom = new Basic.Chatroom();

        private string name;

        public void CreateChatroomMode()
        {
            UseEscAsExit = false;
            DlgActive.View();
            CreateRoom();
            ApiGetChatroomByName();

            _EditChatroom = new EditChatroom(LocalChatroom);
            DlgActive.ChangeActive(_EditChatroom.EditChatroomMode, _EditChatroom.DrawEditChatroom);
        }

        public void DrawCreateChatroom()
        {
            Console.Clear();
            Console.WriteLine("");
            DrawMoon();
        }

        private void CreateRoom() //create chatroom
        {
            Console.WriteLine(Gap + "Set name...");
            name = GetMessageFromUser(false);

            if (ApiCreateChatroom(name))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("GG");
                Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.White;
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Failed");
                Console.ReadLine();
                Console.ForegroundColor = ConsoleColor.White;
            }
        }

        private bool ApiCreateChatroom(string name)
        {
            Basic.Chatroom chtrm = new Basic.Chatroom();
            chtrm.IdUser = LocalUser.IdUser;
            chtrm.Name = name;
            
            HttpResponseMessage response = client.PostAsJsonAsync("http://localhost:60284/api/Chat/CreateChatroom", chtrm).Result; //validation
            if (!response.IsSuccessStatusCode) //failed
                return false;              //Try Again
            else                              //gg
                return true;
        }

        private void ApiGetChatroomByName() //get id chatroom by name
        {
            string JsonString;
            using (WebClient wc = new WebClient())

                JsonString = wc.DownloadString("http://localhost:60284/api/Chat/GetChatroomByName/" + name);
            Basic.Chatroom Chtrm = JsonConvert.DeserializeObject<Basic.Chatroom>(JsonString);
            LocalChatroom = Chtrm;
        }
    }
}

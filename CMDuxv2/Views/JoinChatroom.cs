﻿using CMDuxv2.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CMDuxv2.Basic
{
    public class JoinChatroom : Controller
    {
        private List<Basic.Chatroom> ChtrmList;
        private List<string> Chtrms = new List<string>();
        private bool NextViewIsChat;

        public JoinChatroom(bool IsNextViewChat)
        {
            UseEscAsExit = false;
            NextViewIsChat = IsNextViewChat;
            ChtrmList = ApiChatroomList();
            ChatroomListToString();
        }

        public void JoinChatRoomMode()
        {
            DlgActive.View();
            
            HighlightChange = false;
            SelectHighlight = false;

            while (!HighlightChange)
            {
                if (SelectHighlight)
                {
                    if(NextViewIsChat)
                    {
                        _Chat = new Chat(GetHighlightedChatId());
                        DlgActive.ChangeActive(_Chat.ChatMode, _Chat.DrawChat);
                    }
                    if(!NextViewIsChat)
                    {
                        _EditChatroom = new EditChatroom(GetHighlightedChatId());
                        DlgActive.ChangeActive(_EditChatroom.EditChatroomMode, _EditChatroom.DrawEditChatroom);
                    }
                }
            }
        }
        
        public void DrawJoinChatRoom()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Gap + "Your Chatrooms");
            Console.ForegroundColor = ConsoleColor.White;
            Console.SetCursorPosition(0, Console.CursorTop + 2);

            RecalculateHighlight(Chtrms.Count);
            DrawOptions(Chtrms);
        }

        private Chatroom GetHighlightedChatId()
        {
            int i = 0;
            foreach (Basic.Chatroom item in ChtrmList)
            {
                if (i == HighlightAtIndex)
                    return item;
                i++;
            }
            Chatroom a = new Chatroom();
            return a;
        }


        private List<Basic.Chatroom> ApiChatroomList() // dostane * chatroomy podle ID
        {
            string JsonString;
            using (WebClient wc = new WebClient())
                JsonString = wc.DownloadString("http://localhost:60284/api/Chat/GetChatroomByUser/" + LocalUser.IdUser.ToString()); //here might be bug

            List<Basic.Chatroom> list = new List<Basic.Chatroom>();
            list = JsonConvert.DeserializeObject<List<Basic.Chatroom>>(JsonString);
            return list;
        }

        private void ChatroomListToString()
        {
            foreach (Basic.Chatroom Item in ChtrmList)
            {
                Chtrms.Add(Item.IdChatroom + " " + Item.Name);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CMDuxv2.Basic
{
    public class CrossRoad : Controller
    {
        List<string> opt = new List<string> { "JOIN CHATROOM       ", "CREATE CHATROOM     ", "EDIT CHATROOM       ", "FRIENDS             " };
        private HttpClient client = new HttpClient();

        public void CrossRoadMode()
        {
            DlgActive.View();

            //bool reset
            UseEscAsExit = true;
            HighlightChange = false;
            SelectHighlight = false;

            while (!HighlightChange)
            {
                if (SelectHighlight)
                {
                    SelectHighlight = false;
                    if (HighlightAtIndex == 0)
                    {
                        _JoinChatroom = new JoinChatroom(true);
                        DlgActive.ChangeActive(_JoinChatroom.JoinChatRoomMode, _JoinChatroom.DrawJoinChatRoom);
                    }
                    else if (HighlightAtIndex == 1)
                    {
                        DlgActive.ChangeActive(_CreateChatroom.CreateChatroomMode, _CreateChatroom.DrawCreateChatroom);
                    }
                    else if (HighlightAtIndex == 2)
                    {
                        _JoinChatroom = new JoinChatroom(false);
                        DlgActive.ChangeActive(_JoinChatroom.JoinChatRoomMode, _JoinChatroom.DrawJoinChatRoom);
                    }
                    else if (HighlightAtIndex == 3)
                    {
                        _Friends = new Friends();
                        DlgActive.ChangeActive(_Friends.FriendsMode, _Friends.DrawFriends);
                    }
                }
            }
            HighlightChange = false;
        }

        
        public void DrawCrossRoad()
        {
            Console.Clear();
            Console.WriteLine("");
            DrawMoon();
            RecalculateHighlight(this.opt.Count());
            DrawOptions(this.opt);
        }
    }
}

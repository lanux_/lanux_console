﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WindowsInput;
using WindowsInput.Native;

namespace CMDuxv2.Basic
{
    public class Login : Controller
    {
        private HttpClient client = new HttpClient();


        public void LogInMode()
        {
            DlgActive.View();
            if(LogIn())
            {
                DlgActive.ChangeActive(_CrossRoad.CrossRoadMode, _CrossRoad.DrawCrossRoad);
            }
        }


        public void DrawLogIn()
        {
            Console.Clear();
            Console.WriteLine("");
            DrawMoon();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("                                                      Hint:   User: Karel");
            Console.WriteLine("                                                              Passwd: Abc123");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("");
        }

        private bool LogIn() // Log In
        {
            User u = new User();
            Console.WriteLine(Gap + "Please insert username...");
            u.Username =  GetMessageFromUser(false);
            Console.WriteLine(Gap + u.Username);

            Console.WriteLine(Gap + "Please use your password...");
            u.Password = GetMessageFromUser(true);

            return ApiLogIn(u);
        }

        private bool ApiLogIn(User u)
        {
            HttpResponseMessage response = client.PostAsJsonAsync("http://localhost:60284/api/Account/Login", u).Result; //validation
            if (!response.IsSuccessStatusCode) //Login failed
            {
                return false;              //Try Again
            }
            else                              //valid login
            {
                LocalUser = ApiCurrentUser(u.Username);
                return true;
            }
        }

        private User ApiCurrentUser(string username) //Download remaining data about local logged user
        {
            string JsonString;

            using (WebClient wc = new WebClient())
            {
                JsonString = wc.DownloadString("http://localhost:60284/api/Account/CurrentUser/" + username);
            }

            User _crnt = new User();
            _crnt = JsonConvert.DeserializeObject<User>(JsonString);
            return _crnt;
        }
    }
}

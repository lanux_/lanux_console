﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;

namespace CMDuxv2.Basic
{
    public class SignUp : Controller
    {
        private HttpClient client = new HttpClient();

        public void SignUpMode()
        {
            DlgActive.View();
            if(RegistationController())
                DlgActive.ChangeActive(_CrossRoad.CrossRoadMode, _CrossRoad.DrawCrossRoad);
        }
        public void DrawSignUp()
        {
            Console.Clear();
            Console.WriteLine("");
            DrawMoon();
            Console.WriteLine("");
        }

        private bool RegistationController()
        {
            User u = CreateNewUser();       //create user
            if(u.Password != null)
            {
                if (ApiSendUser(u))            //send user
                {
                    if (ApiLogIn(u))           //LogIn User
                        return true;
                    else
                        Console.WriteLine(Gap + "Login Failed, but user is registred");
                }
                else
                    Console.WriteLine(Gap + "Send user to API failed");
            }
            Console.WriteLine(Gap + "Try again");
            return false;
        }
        private bool ApiSendUser(User u) //send new user to API
        {
            HttpResponseMessage response = client.PostAsJsonAsync("http://localhost:60284/api/Account/Register", u).Result;
            if (!response.IsSuccessStatusCode) //register failed
                return false;              //Try Again

            else                              //valid user
                return true;
        }

        private bool ApiLogIn(User u) // Log In
        {
            HttpResponseMessage response = client.PostAsJsonAsync("http://localhost:60284/api/Account/Login", u).Result; //validation
            if (!response.IsSuccessStatusCode) //Login failed
                return false;              //Try Again
            else                              //valid login
            {
                LocalUser = ApiCurrentUser(u.Username);
                return true;
            }
        }

        private User ApiCurrentUser(string username) //Download remaining data about local logged user
        {
            string JsonString;

            using (WebClient wc = new WebClient())
                JsonString = wc.DownloadString("http://localhost:60284/api/Account/CurrentUser/" + username);

            User _crnt = new User();
            _crnt = JsonConvert.DeserializeObject<User>(JsonString);
            return _crnt;
        }

        private Basic.User CreateNewUser() // Create new user
        {
            User u = new User();
            Console.WriteLine(Gap + "Username");
            u.Username = GetMessageFromUser(false);
            ClearCurrentConsoleLine();
            Console.WriteLine(Gap + u.Username);
            Console.WriteLine(Gap + "Nickname");
            u.Nickname = GetMessageFromUser(false);
            ClearCurrentConsoleLine();
            Console.WriteLine(Gap + u.Nickname);

            Console.Write(Gap + "Password  =  ");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Gap + "6-15 long, min 1num, 1UpperCase 1LowerCase");
            Console.ForegroundColor = ConsoleColor.White;
            string passwd1 = GetMessageFromUser(true);
            ClearCurrentConsoleLine();
            Console.WriteLine(Gap + HidePasswd(passwd1));

            Console.WriteLine(Gap + "Password again...");
            string passwd2 = GetMessageFromUser(true);
            ClearCurrentConsoleLine();
            Console.WriteLine(Gap + HidePasswd(passwd2));

            if(PasswordValidation(passwd1, passwd2))
                u.Password = passwd1;

            Console.ForegroundColor = ConsoleColor.White;
            return u;
        }

        private string HidePasswd(string passwd)
        {
            string p = "";

            for (int i = 0; i < passwd.Length; i++)
                p += "*";

            return p;
        }

        private bool PasswordValidation(string psswd1, string psswd2)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            if (psswd1 != psswd2)
            {
                Console.WriteLine(Gap + "Passwords doesn't match");
                Console.ReadLine();
                return false;
            }

            
            Regex regex = new Regex(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{6,15}$");
            if (regex.Match(psswd1).Success)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine(Gap + "Your password is valid");
                Console.ReadLine();
                return true;
            }
            Console.WriteLine(Gap + "Regex doesn't match with your password");
            Console.ReadLine();
            return false;
        }
    }
}

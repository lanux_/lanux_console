﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMDuxv2.Basic
{
    public class Message
    {
        public int IdMessage { get; set; }

        public int IdChatroom { get; set; }

        public int IdUser { get; set; }

        public string Nickname { get; set; }

        public string Content { get; set; }

        public string Time { get; set; }
    }
}

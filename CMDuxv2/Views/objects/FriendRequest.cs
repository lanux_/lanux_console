﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMDuxv2.Views.objects
{
    public class FriendRequest
    {
        public int IdUser { get; set; }

        public string FUsername { get; set; }
    }
}

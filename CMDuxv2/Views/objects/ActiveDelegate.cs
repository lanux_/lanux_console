﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMDuxv2.Views.objects
{
    public class ActiveDelegate : Controller
    {
        public delegate void DlgActiveMode();
        public delegate void DlgActiveView();

        public DlgActiveMode Mode { get; set; }
        public DlgActiveView View { get; set; }

        public ActiveDelegate()
        {
            ChangeActive(_MainMenu.MainMenuMode, _MainMenu.DrawMainMenu);
        }

        public void ChangeActive(DlgActiveMode mode, DlgActiveView view)
        {
            this.Mode = mode;
            this.View = view;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMDuxv2.Basic
{
    public class Chatroom
    {
        public int IdChatroom { get; set; }

        public int IdUser { get; set; }

        public string Name { get; set; }
    }
}

﻿using CMDuxv2.Basic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CMDuxv2.Views
{
    public class EditChatroom : Controller
    {
        private HttpClient client = new HttpClient();

        List<string> opt = new List<string> { "ADD USER    ", "OK          "};
        List<int> ListOfUsersToSend = new List<int>(); //added users by user

        List<User> FriendList;

        List<User> UsersInChatroom; //those users were already in this chatroom
        List<User> CompleteList = new List<User>(); //complete list of users related to this chttrm
        List<int> UsersInChatroomId = new List<int>();
        
        private Chatroom LocalChtrm;

        public EditChatroom(Chatroom chtrm)
        {
            UseEscAsExit = false;
            LocalChtrm = chtrm;
            ListOfUsersToSend.Add(LocalChtrm.IdChatroom);
            ApiFriendList();
            ApiGetUsersByChatroom();
            FillCompleteList();
            FillUsersInChatroomId();
        }

        public void EditChatroomMode()
        {
            DlgActive.View();
            HighlightChange = false;
            SelectHighlight = false;

            while (!HighlightChange)
            {
                if (SelectHighlight)
                {
                    if (HighlightAtIndex == 0)
                    {
                        GetFriendIdFromUser();
                    }
                    else if(HighlightAtIndex == 1)
                    {
                        ApiSendUsersToChatroom();
                        DlgActive.ChangeActive(_CrossRoad.CrossRoadMode, _CrossRoad.DrawCrossRoad);
                        DlgActive.View();
                    }
                }
            }
        }

        public void DrawEditChatroom()
        {
            Console.Clear();
            DrawUserList();
            Console.WriteLine();
            RecalculateHighlight(this.opt.Count());
            DrawOptions(opt);
        }


        // |                         |
        // |   Add user to chatroom  |
        // V                         V
        

        private void GetFriendIdFromUser() //add friend to local list
        {
            Console.WriteLine("Please insert your friend's Id...");
            try
            {
                int FId = Convert.ToInt32(GetMessageFromUser(false));
                ClearCurrentConsoleLine();
                FIdValidation(FId);
                Console.WriteLine(Gap + "Id does not exist");
            }
            catch
            {
                Console.WriteLine(Gap + "Wrong format... Geve me just simple number...");
            }
        }

        // |                         |
        // |      User Validation    |
        // V                         V
        private void FIdValidation(int id) //validate friends id if exists etc...
        {
            if (!IsUserInFList(id))
            {
                if (!DoesUserExists(id))
                    Console.WriteLine("User is not in your friendlist, or does not exist");
                else
                {
                    ListOfUsersToSend.Add(id);
                    UsersInChatroomId.Add(id);
                }
            }
        }

        private bool IsUserInFList(int id) //jestli je user uz v seznamu lidi co poslat, tak to je spatne
        {
            foreach (int item in ListOfUsersToSend.Skip(1))
            {
                if (item == id)
                {
                    Console.WriteLine("This User already in chatroom");
                    Console.ReadLine();
                    return true;
                }
            }
            return false;
        }

        private bool DoesUserExists(int id)
        {
            foreach (Basic.User item in CompleteList)
            {
                if (item.IdUser == id)
                {
                    return true;
                }
            }
            return false;
        }


        // |                         |
        // |  Draw User List methods |
        // V                         V
        private void DrawUserList()
        {
            Console.WriteLine("Users");
            Console.WriteLine();
            DrawUsers();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
        }

        private void DrawUsers()
        {
            foreach (Basic.User item in CompleteList)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                string IdName = item.IdUser + "-" + item.Username;
                int _gap = 20 - IdName.Length;

                if(IsUserInChatroom(item.IdUser))
                    Console.ForegroundColor = ConsoleColor.Green;

                Console.Write(IdName + GapByLenght(_gap));
            }
        }

        private bool IsUserInChatroom(int uId)
        {
            foreach (int Item in UsersInChatroomId)
            {
                if (uId == Item)
                    return true;
            }
            return false;
        }


        private void FillCompleteList() //fills complete list I have to use 3 foreaches, just becouse of duplicates
        {
            foreach (User item in UsersInChatroom)
                CompleteList.Add(item);

            foreach (User item in FriendList)
            {
                if(!IsFriendInChatroom(item))
                    CompleteList.Add(item);
            }

            //CompleteList = new List<User>(FriendList.Count + UsersInChatroom.Count);
            //CompleteList.AddRange(FriendList);
            //CompleteList.AddRange(UsersInChatroom);
        }

        private bool IsFriendInChatroom(User friend)
        {
            foreach (User item in UsersInChatroom)
            {
                if (friend == item)
                    return true;
            }
            return false;
        }
        private void FillUsersInChatroomId()
        {
            foreach (User item in UsersInChatroom)
            {
                UsersInChatroomId.Add(item.IdUser);
            }
        }

        // |                         |
        // |       Api methods       |
        // V                         V

        private void ApiFriendList() //get friendlist
        {
            string JsonString;
            using (WebClient wc = new WebClient())
                JsonString = wc.DownloadString("http://localhost:60284/api/Account/GetFriends/" + LocalUser.IdUser); //here might be bug

            FriendList = JsonConvert.DeserializeObject<List<Basic.User>>(JsonString);
        }

        private void ApiGetUsersByChatroom() // get all chatrooms by ID
        {
            string JsonString;
            using (WebClient wc = new WebClient())
                JsonString = wc.DownloadString("http://localhost:60284/api/Chat/UsersByChatroom/" + LocalChtrm.IdChatroom.ToString()); //here might be bug

            UsersInChatroom = JsonConvert.DeserializeObject<List<User>>(JsonString);
        }

        private bool ApiSendUsersToChatroom()
        {
            HttpResponseMessage response = client.PostAsJsonAsync("http://localhost:60284/api/Chat/AddToChatroom", ListOfUsersToSend).Result; //validation
            if (!response.IsSuccessStatusCode) //failed
                return false;              //Try Again
            else                              //gg
                return true;
        }
    }
}

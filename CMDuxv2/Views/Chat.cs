﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CMDuxv2.Basic
{
    public class Chat : Controller
    {

        private HttpClient client = new HttpClient();
        private List<Basic.Message> LocalMsgs;
        private Chatroom Localchtrm;

        public Chat(Chatroom chtrm)
        {
            Localchtrm = chtrm;
            LocalMsgs = ApiGetMessageListById(chtrm.IdChatroom);
        }

        public void ChatMode()
        {
            DlgActive.View();
            Thread ThreadUpdate = new Thread(new ThreadStart(ApiUpdate));
            ThrdUpdate = true;
            ThreadUpdate.Start();

            BoolChat = true;
            while(BoolChat)
            {
                string MsgContent = GetMessageFromUser(false);
                ApiSendMessage(MsgContent);
            }

            BoolChat = false;
            DlgActive.ChangeActive(_CrossRoad.CrossRoadMode, _CrossRoad.DrawCrossRoad);
        }

        public void DrawChat()
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(Gap + "ChatroomId-" + Localchtrm.IdChatroom + "       Name: " + Localchtrm.Name + "       AdminId: " + Localchtrm.IdUser + "\n");
            Console.ForegroundColor = ConsoleColor.White;
            

            foreach (Basic.Message item in LocalMsgs.Skip(LocalMsgs.Count - 25)) // here might be mistake
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.Write(Gap + item.Time + " ");
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write(item.Nickname);

                Console.ForegroundColor = ConsoleColor.White;
                Console.Write(":  ");
                Console.WriteLine(item.Content);
                //ContentBuilder(item.Content);
            }
            Console.WriteLine("______________________________________________________________");

            if(KeyboardOutput != "")
                Console.Write(Gap + KeyboardOutput);
        }

        private void ContentBuilder(string content)
        {
            if(content.Length > 45)
            {
                //content.TrimEnd(content.Length - 45);
                content.EndsWith(content[45].ToString());
            }
            Console.WriteLine(content);
        }

        private void ApiSendMessage(string content) //send message
        {
            Basic.Message msg = new Basic.Message { IdChatroom = Localchtrm.IdChatroom, Content = content, Time = DateTime.Now.ToString(), IdUser = LocalUser.IdUser };
            HttpResponseMessage response = client.PostAsJsonAsync("http://localhost:60284/api/Chat/SendMessage", msg).Result;

            if (response.IsSuccessStatusCode)
                DlgActive.View();
            else
                Debug.WriteLine(Gap + "SendMessage() failed");
        }

        private List<Basic.Message> ApiGetMessageListById(int idChatroom) // dostane * message podle IDChatroomu
        {
            string JsonString;
            using (WebClient wc = new WebClient())
                JsonString = wc.DownloadString("http://localhost:60284/api/Chat/GetMessages/" + idChatroom.ToString());

            List<Basic.Message> list = new List<Basic.Message>();
            list = JsonConvert.DeserializeObject<List<Basic.Message>>(JsonString);
            return list;
        }

        private void ApiUpdate() //update list of messages
        {
            while(ThrdUpdate)
            {
                Thread.Sleep(500);
                List<Basic.Message> newMsgs = ApiGetMessageListById(Localchtrm.IdChatroom);
                if (newMsgs.Count != LocalMsgs.Count)  //na api udelat metodu, kdy se vrati msgs.count
                {
                    LocalMsgs = newMsgs;
                    DlgActive.View();
                }
            }
        }
    }
}

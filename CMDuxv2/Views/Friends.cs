﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace CMDuxv2.Basic
{
    public class Friends : Controller
    {
        private HttpClient client = new HttpClient();
        private List<Basic.User> FriendList;
        private List<Basic.User> UserList;
        List<string> opt = new List<string>() { "ADD FRIEND ", "BACK       " };

        public Friends()
        {
            ApiGetAllUsers();
            ApiFriendList();
        }

        public void FriendsMode()
        {
            UseEscAsExit = false;
            
            HighlightChange = false;
            SelectHighlight = false;
            DlgActive.View();


            while (!HighlightChange)
            {
                if (SelectHighlight)
                {
                    if (HighlightAtIndex == 0)
                    {
                        AddFriend();
                    }
                    else
                    {
                        DlgActive.ChangeActive(_CrossRoad.CrossRoadMode, _CrossRoad.DrawCrossRoad);
                    }
                }
            }
        }

        public void DrawFriends()
        {
            Console.Clear();
            DrawUsers();
            RecalculateHighlight(this.opt.Count());
            DrawOptions(opt);
        }

        // |                         |
        // |   get userid from user  |
        // V                         V

        private bool AddFriend() //Wizzard for adding a friend
        {
            Console.WriteLine(Gap + "Your friend's Username?");
            string Fname = GetMessageFromUser(false);

            if (Fname == LocalUser.Username)
            {
                Console.WriteLine(Gap + "Roses are red, violets are blue, I'm schizophrenic, and so am I.");
                Console.ReadLine();
                return false;
            }

            ClearCurrentConsoleLine();
            Console.WriteLine(Gap + Fname);

            if (Fname != "")
            {
                if (ApiAddFriend(Fname))
                {
                    ApiFriendList();
                    return true;

                }
                else
                {
                    Console.WriteLine(Gap + "Invalid username");
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine(Gap + "You have to type something...");
                Console.ReadLine();
                return false;
            }
            return false;
        }

        // |                         |
        // |  Draw User List methods |
        // V                         V

        private void DrawUsers()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("Users");
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write(Gap + "friends");
            Console.WriteLine();
            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.White;
            WriteAndMarkUsers();
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
        }

        private void WriteAndMarkUsers()
        {
            foreach (Basic.User item in UserList)
            {
                Console.ForegroundColor = ConsoleColor.Red;

                string IdName = item.IdUser + "-" + item.Username;
                int _gap = 20 - IdName.Length;

                if (IsUserYourFriend(item.IdUser))
                    Console.ForegroundColor = ConsoleColor.Green;

                Console.Write(IdName + GapByLenght(_gap));
            }
        }
        private bool IsUserYourFriend(int uId)
        {
            foreach (User Item in FriendList)
            {
                if (uId == Item.IdUser)
                    return true;
            }
            return false;
        }

        // |                         |
        // |       Api methods       |
        // V                         V

        private bool ApiAddFriend(string FriendsUsername) //api post add friend ///////////////////////////doesnt work///////////////////////////////////////////////////////
        {
            Views.objects.FriendRequest fr = new Views.objects.FriendRequest();
            fr.IdUser = LocalUser.IdUser;
            fr.FUsername = FriendsUsername;



            HttpResponseMessage response = client.PostAsJsonAsync("http://localhost:60284/api/Account/MakeFriend/", fr).Result; //validation
            if (!response.IsSuccessStatusCode) //failed
            {
                return false;              //Try Again
            }
            else
                return true;
        }

        private void ApiFriendList() //get friendlist
        {
            string JsonString;
            using (WebClient wc = new WebClient())
                JsonString = wc.DownloadString("http://localhost:60284/api/Account/GetFriends/" + LocalUser.IdUser); //here might be bug
            
            FriendList = JsonConvert.DeserializeObject<List<Basic.User>>(JsonString);
        }

        private void ApiGetAllUsers() //get all users
        {
            string JsonString;
            using (WebClient wc = new WebClient())
                JsonString = wc.DownloadString("http://localhost:60284/api/Account"); //here might be bug
            UserList = JsonConvert.DeserializeObject<List<Basic.User>>(JsonString);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CMDuxv2.Basic
{
    public class MainMenu : Controller
    {
        List<string> opt = new List<string> { "LOG IN    ", "SIGN UP   " };

        public void MainMenuMode()
        {
            DlgActive.View();
            HighlightChange = false;
            SelectHighlight = false;

            while (!HighlightChange)
            {
                if (SelectHighlight)
                {
                    if (HighlightAtIndex == 0)
                    {
                        DlgActive.ChangeActive(_Login.LogInMode, _Login.DrawLogIn);
                    }
                    else
                    {
                        DlgActive.ChangeActive(_SignUp.SignUpMode, _SignUp.DrawSignUp);
                    }
                }
            }
        }

        public void DrawMainMenu()
        {
            Console.Clear();
            Console.WriteLine("");
            DrawMoon();
            Console.WriteLine("");
            Console.WriteLine("                                                      _C_O_N_T_R_O_L_S_ \n" +
                              "                                                     │ ARROWS - Move   │\n" +
                              "                                                     │ ESCAPE - Escape │\n" +
                              "                                                     │  ENTER - select │\n" +
                              "                                                     └─────────────────┘\n \n");

            RecalculateHighlight(this.opt.Count());
            DrawOptions(opt);
        }
    }
}
